mysql_host = "127.0.0.1"
mysql_username = "root"
mysql_password = ""
mysql_port = "3306"
mysql_db = "lesrp"
MYSQL = nil

function OnGameModeInit()
	MYSQL = dbConnect( "mysql", "dbname="..mysql_db..";host="..mysql_host..";port="..mysql_port..";unix_socket=/var/run/mysqld/mysqld.sock", mysql_username, mysql_password )
	if MYSQL then outputDebugString("[MYSQL CONNECTED]")
	else outputDebugString("[ERROR MYSQL CONNECT]") end
	singleQuery("SET NAMES 'utf8'")
	singleQuery("SET CHARACTER SET 'utf8'")
end
addEventHandler ( "onResourceStart", resourceRoot, OnGameModeInit )

function OnGameModeExit()
	if MYSQL then destroyElement(MYSQL) end
end
addEventHandler ( "onResourceStop", resourceRoot, OnGameModeExit )

function getConnection()
	return MYSQL
end

function sqlQuery(str,...) 
	if(MYSQL) then
		local query = dbQuery(MYSQL, str,...)
		local result, numrows = dbPoll(query, -1)
		if(result and numrows > 0) then
			return result, numrows
		else
			return false;
		end
	else
		return false;
	end
end

function singleQuery(str,...)
	if(MYSQL) then
		local query = dbQuery(MYSQL,str,...)
		local result = dbPoll(query,-1)
		if (type(result == "table")) then
			return result[1]
		else
			return result
		end
	else
		return false
	end
end

function execute(str,...)
	if (MYSQL) then
		local query = dbExec(MYSQL,str,...)
		return query
	else
		return false
	end
end