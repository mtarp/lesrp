f_model = {
	{ 1, "bfyst", "bfyst", "Курица" },
	{ 2, "bmost", "bmost", "Лиса" },
	{ 1854, "kudah", "bfyst", "Курица в зубах" },
	{ 7, "bmyboun", "bmyboun", "Добераман" }
}


function replaceModel( resource )
	if getResourceName( getThisResource() ) ~= getResourceName( resource ) then return end
	for i = 1, #f_model, 1 do
		local txd = engineLoadTXD ( "model/"..f_model[i][3]..".txd" )
		engineImportTXD ( txd, f_model[i][1] )
		local dff = engineLoadDFF ( "model/"..f_model[i][2]..".dff")
		engineReplaceModel ( dff, f_model[i][1] )
	end
end
addEventHandler( "onClientResourceStart", root, replaceModel )