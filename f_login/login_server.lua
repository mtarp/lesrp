
spawn_position = {
	{}, -- Курицы
	{ 919.7, -220.9, 29.7 }, -- Лисы
	{}, {}, {}, {},
	{ 1100.7, -326.1, 74.0}, -- Собаки
}

function join_player()
	source:setCameraMatrix( 1007.0, -355.0, 90.0, 1065.0, -324.0, 80.0 )
	source:fadeCamera( true, 2.0 )
	triggerClientEvent( source, "join_player_client", source )
	showChat( source, false )
	showCursor( source, true )
	source:setHudComponentVisible( "all", false )
end
addEventHandler( "onPlayerJoin", getRootElement(), join_player )


function join_player_test( source )
	source:setCameraMatrix( 1007.0, -355.0, 90.0, 1065.0, -324.0, 80.0 )
	source:fadeCamera( true, 2.0 )
	triggerClientEvent( source, "join_player_client", source )
	showChat( source, false )
	showCursor( source, true )
	source:setHudComponentVisible( "all", false )
end
addCommandHandler( "test_login", join_player_test )

--***********************************************
function loginHandler ( username, password )
	if exports.f_db:singleQuery("SELECT * FROM `players` WHERE `name` = ? LIMIT 1", string.lower( username ) ) then
		local user = exports.f_db:singleQuery( 
			"SELECT * FROM `players` WHERE `name` = ? AND `password` = ? LIMIT 1",
			string.lower( username ), password )
		if user then
			if user.race == 0 then 
				outputDebugString( "START UCP")
			else
				source:spawn( unpack( spawn_position[user.race] ) )
				source:setModel( user.race )
				source:setCameraTarget( source )
			end
			triggerClientEvent( source, "close_login_client", source )
			outputChatBox ( "Поздравляем, вы успешно вошли.", source )
		else
			outputChatBox("Неправильные логин и пароль. Попробуйте еще раз.", source)
		end
	else
		outputChatBox("Данный аккаунт не найден в базе данных. Попробуйте еще раз.", source)
	end
end

addEvent( "submitLogin", true )
addEventHandler( "submitLogin", getRootElement(), loginHandler )