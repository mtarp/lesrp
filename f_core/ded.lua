
local ded = nil
local ded_col = nil
local ded_fire = nil

function dedCore( resource )
	if getResourceName( getThisResource() ) ~= getResourceName( resource ) then return end
	ded = Ped.create( 161, 1106.759765625, -315.8349609375, 74.7421875 )
	ded_col = ColShape.Sphere( 1106.759765625, -315.8349609375, 74.7421875, 20 )
	setTimer( 
		function()
			giveWeapon( ded, 25, 1000, true )
			exports.npc_hlc:enableHLCForNPC( ded, "walk", 1, 20 )
			exports.npc_hlc:setNPCWeaponAccuracy( ded, 1 )
		end, 3000, 1 
	)	
	addEventHandler( "onColShapeHit", ded_col, ded_hit )
	addEventHandler( "onColShapeLeave", ded_col, ded_leave )
end
addEventHandler( "onResourceStart", root, dedCore )

function ded_hit( player, dim )
	if ded_fire == nil and player:getModel() == 2 then 
		ded_fire = player
		exports.npc_hlc:addNPCTask( ded, {"shootElement", player } )
	end
end

function ded_leave( player, dim )
	if ded_fire == player then 
		ded_fire = nil 
		exports.npc_hlc:clearNPCTasks( ded )
	end
end

