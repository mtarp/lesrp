
local chicken = { 1116.1806640625, -315.5244140625, 73.99218755 }
local round_chicken = 10

addEventHandler("onResourceStart", root, 
	function( resource )
		if getResourceName( getThisResource() ) ~= getResourceName( resource ) then return end
		for _, player in ipairs( getElementsByType ( "player" ) ) do 
			col = createColSphere( 0, 0, 0, 1.0 )
			col:attach( player )
			col:setData( "player", player )
			player:setData( "death", false )
			player:setData( "kur", false )
			player:setNametagShowing( false )
		end
		
		for i = 1, tonumber( round_chicken ), 1 do 
			kur = createPed( 1, 
				chicken[1] + math.random( -2, 2 ), 
				chicken[2] + math.random( -2, 2 ), 
				chicken[3] + math.random( 0, 2 ), math.random( 0, 359 ), true )
			setTimer( 
				function( element )
					
				end, 5000, 1, kur 
			)	
			--[[
			local blip = Blip( chicken[1], chicken[2], chicken[3] )
			blip:attach( kur )]]
			col = createColSphere( chicken[1], chicken[2], chicken[3], 1.5 )
			col:attach( kur )
			col:setData( "object", kur )
			exports.npc_hlc:enableHLCForNPC( kur, "walk", 1, 20 )
			exports.npc_hlc:addNPCTask( kur, 
				{ "walkToPos", 1109.1 + math.random( 0, 9 ), -305 - math.random( 0, 9 ), 20.0, 1.0 } 
			)
			addEventHandler( "onColShapeHit", col, hitChecken )
			addEventHandler( "npc_hlc:onNPCTaskDone", kur, moveChicken )
		end
		
	end
)
	
function moveChicken( task )
	if source:getData( "in_fox" ) == true then 
		exports.npc_hlc:disableHLCForNPC( source )
		setTimer( 
			function( ped ) 
				outputDebugString("STEART")
				exports.npc_hlc:enableHLCForNPC( ped, "walk", 1, 20 ) 
				exports.npc_hlc:addNPCTask( ped, 
					{ "walkToPos", 1109.1 + math.random( 0, 9 ), -305 - math.random( 0, 9 ), 20.0, 1.0 } 
				)
			end, 
			2500, 1, source )
		source:setData( "in_fox", false )
	end
		
	exports.npc_hlc:addNPCTask( source, 
		{ "walkToPos", 1109.1 + math.random( 0, 9 ), -305 - math.random( 0, 9 ), 20.0, 1.0 } 
	)
end

function hitChecken( theElement, matchingDimension )
	if theElement:getModel() == 2 and theElement:getData( "kur" ) == false 
		and theElement:getData( "death" ) == false then 
		theElement:setData( "kur", true )
		
		source:getData( "object" ):destroy()
		source:destroy()
		
		local p = theElement:getPosition()
		local object = createObject( 1854, p.x, p.y, p.z )
		object:setScale( 0.3 )
		exports.bone_attach:attachElementToBone( object, theElement, 4, -0.15, 0.1, 1.1, 0, 90, 0 )
		theElement:setData( "kur", true )
		theElement:setData( "kur_object", object )
	end
end