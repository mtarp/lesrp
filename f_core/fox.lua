local marker = {
	{ 1052.8046875, -234.65625, 70.169174194336 },
	{ 1073.0673828125, -231.546875, 73.614646911621 },
	{ 1090.19921875, -228.7265625, 73.024261474609 },
}

addEventHandler( "onResourceStart", root, 
	function( resource )
		if getResourceName( getThisResource() ) ~= getResourceName( resource ) then return end
		
		for i = 1, #marker, 1 do 
			local mark = createMarker( marker[i][1], marker[i][2], marker[i][3], "checkpoint", 
				2.0, 255, 0, 0, 255 )
			addEventHandler( "onMarkerHit", mark, MarkerHit )	
		end
	end
)

function MarkerHit( player, dim )
	if player:getData( "kur" ) == true then 
		player:getData( "kur_object" ):destroy()
		player:setData( "kur_object", false )
		player:setData( "kur", false )
	end	
end
addEventHandler( "onMarkerHit", root, MarkerHit )

function reloadACL ( source, command )
-- Check if they're an admin...
	if ( isObjectInACLGroup ( "user." .. getAccountName ( getPlayerAccount ( source )), aclGetGroup ( "Admin" ) ) ) then
		local reload = aclReload() -- Reload the ACL
			if ( reload ) then -- Check it was reloaded successfully
				outputChatBox ( "ACL was successfully reloaded.", source, 255, 0, 0 ) -- If so, output it
			else -- If not, output it (line below)
				outputChatBox ( "An unknown error occured. Please check the ACL file exists.", source, 255, 0, 0 )
			end
	else -- If they're not an admin, output it (below)
		outputChatBox ( "You must be an admin to use this command!", source, 255, 0, 0 )
	end
end
addCommandHandler ( "reloadACL", reloadACL )